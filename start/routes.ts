/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

// Venues routes
Route
  .resource('venues', 'VenuesController')
  .apiOnly()
  .middleware({
    '*':['auth', 'verify'],
    'store': 'owner',
    'update': 'owner',
    'destroy': 'owner'
  })
Route
  .post('/venues/:id/fields', 'VenuesController.addField')
  .middleware(['auth','verify','owner'])
  .as('venues.addField')
Route
  .post('/venues/:id/bookings','VenuesController.makeSchedule')
  .middleware(['auth', 'verify', 'user'])
  .as('venues.makeSchedule')

// Fields routes
Route
  .resource('fields', 'FieldsController')
  .apiOnly()
  .middleware({
    '*': ['auth', 'verify'],
    'update': 'owner',
    'destroy': 'owner'
  })
  .except(['store'])

// Bookings routes
Route
  .resource('bookings', 'BookingsController')
  .apiOnly()
  .middleware({
    '*': ['auth', 'verify'],
    'store': 'user'
  })
  .except(['destroy','update','store'])
Route
  .put('/bookings/:id/join', 'BookingsController.join')
  .middleware(['auth','verify','user'])
  .as('bookings.join')
Route
  .put('/bookings/:id/unjoin', 'BookingsController.unjoin')
  .middleware(['auth','verify','user'])
  .as('bookings.unjoin')
Route
  .get('schedules', 'BookingsController.schedule')
  .middleware(['auth','verify','user'])
  .as('bookings.schedule')

// Auth routes
Route.post('/register', 'AuthController.register').as('auth.register')
Route.post('/login', 'AuthController.login').as('auth.login')
Route.post('/otp-confirmation', 'AuthController.otpConfirmation').as('auth.verifyotp')
