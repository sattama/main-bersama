import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator'
import { schema } from '@ioc:Adonis/Core/Validator'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'

export default class AuthController {

  public async register({request, response}:HttpContextContract) {
    try {
      const data = await request.validate(UserValidator)
      const {email} = data

      const newUser = await User.create(data)

      const otp_code = Math.floor(100000 + Math.random() * 900000)
      await Database.table('otp_codes').insert({otp_code: otp_code, user_id:newUser.id})
      await Mail.send((message) => {
        message
          .from('abduh@sanberdev.com')
          .to(email)
          .subject('Welcome Onboard!')
          .htmlView('emails/otp_verification', {otp_code})
      })
      return response.created({message: 'registered! please check your otp code'})
    } catch(error) {
      return response.unprocessableEntity(error.messages)
    }
  }
  public async login({request,response,auth}:HttpContextContract) {
    const userSchema = schema.create({
      email: schema.string(),
      password: schema.string()
    })
    try {
      const payload = await request.validate({schema: userSchema})
      const {email, password} = await payload

      const token = await auth.use('api').attempt(email, password)

      return response.ok({message: 'Login Succeed', token})
    } catch (error) {
      if (error.guard) {
        return response.badRequest({message: 'Login Error', error: error.message})
      } else {
        return response.badRequest({message: 'Login Error', error: error.messages})
      }
    }
  }

  public async otpConfirmation({request, response}:HttpContextContract) {
    let otp_code = request.input('otp_code')
    let email = request.input('email')

    let user:any = await User.findBy('email', email)
    let otpCheck = await Database.query().from('otp_codes').where('otp_code', otp_code).first()

    if (user?.id === otpCheck?.user_id) {
      user.isVerified= true
      await user?.save()
      return response.ok({message: 'Berhasil konfirmasi OTP'})
    } else {
      return response.badRequest({message: 'Gagal verifikasi OTP'})
    }
  }
}
