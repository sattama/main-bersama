import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import Booking from 'App/Models/Booking'

export default class BookingsController {
  public async index({response}) {
    const bookings = await Booking.all()
    return response.ok(bookings)
  }

  public async show({response, params}:HttpContextContract) {
    const bookingId = parseInt(params.id)

    const data:any = await Booking
      .query()
      .where('id', bookingId)
      .first()

    let booking = {...data.$original, players:[]}


    let players = await Database
      .from('users_has_bookings')
      .select('users_id', 'users.name', 'users.email')
      .where('bookings_id', bookingId)
      .join('users', 'users_has_bookings.users_id', '=', 'users.id')
    players.forEach(player => {
      booking.players.push({...player})
    });

    return response.ok(booking)
  }

  public async join({params, auth, response}:HttpContextContract) {
    const user = auth.user
    const userId:any = user?.id
    const bookingid = params.id

    // Check if user already joined the schedule
    const schedule = await Database
      .from('users_has_bookings')
      .select('*')
      .where('users_id', userId)
      .where('bookings_id', bookingid)

    if (schedule.length === 1) {
      return response.unprocessableEntity({message: "You've already joined the schedule!"})
    }

    await user?.related('bookings').attach([bookingid])
    response.ok({message: "You're successfuly joined to the schedule!"})
  }

  public async unjoin({params, auth, response}:HttpContextContract) {
    const user = auth.user
    const userId:any = user?.id
    const bookingid = params.id

    // Check if user already joined the schedule
    const schedule = await Database
      .from('users_has_bookings')
      .select('*')
      .where('users_id', userId)
      .where('bookings_id', bookingid)

    if (schedule.length === 0) {
      return response.unprocessableEntity({message: "You've have no schedule at the moment! please join the schedule"})
    }

    await user?.related('bookings').detach([bookingid])
    return response.ok({message: "You're successfuly unjoined to the schedule!"})
  }

  public async schedule ({response,auth}:HttpContextContract) {
    const user = auth.user
    const userId:any = user?.id

    const scheduleByUserId = await Database
    .from('users_has_bookings')
    .select('*')
    .where('users_id', userId)

    let bookingIds = scheduleByUserId.map(booking => booking.bookings_id)

    const schedule = await Booking
      .query()
      .where('id', 'in', bookingIds)
    return response.ok(schedule)
  }
}
