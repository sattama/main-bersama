import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
import Venue from 'App/Models/Venue'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
import CreateFieldValidator from 'App/Validators/CreateFieldValidator'

export default class VenuesController {
  public async index({response}:HttpContextContract) {
    let venues = await Venue.all()
    return response.status(200).json(venues)
  }

  public async store({request, response}:HttpContextContract) {
    try {
      const data= await request.validate(CreateVenueValidator)
      const venue = await Venue.create(data)
      return response.created({message: 'Data berhasil ditambahkan!', newId: venue.id})
    } catch (error) {
      return response.badRequest(error.messages)
    }
  }

  public async show({params, response}:HttpContextContract) {
    let id = params.id
    let venue = await Venue.query().preload('fields').where('id', id).first()
    return response.status(200).json(venue);
  }

  public async update({params, response, request}:HttpContextContract) {
    let id = params.id
    try {
      const data = await request.validate(CreateVenueValidator)
      const [affectedRow] = await Venue
        .query()
        .where('id', id)
        .update(data)
      return response.ok({message: "Data berhasil diupdate", data : affectedRow})
    } catch (error) {
      return response.badRequest(error.messages)
    }

  }

  public async destroy({params, response}:HttpContextContract) {
    let id = params.id
    const [affectedRow] = await Venue
      .query()
      .where('id', id)
      .delete()
    return response.ok({message: "Data berhasil dihapus!", data: affectedRow})
  }

  public async addField ({params,request,response}:HttpContextContract) {
    const venue_id = params.id
    try {
      const data = await  request.validate(CreateFieldValidator)
      const venue = await Venue.find(2)
      await venue?.related('fields').create({
        name: data.name,
        type: data.type,
        venueId: venue_id
      })
      return response.created({message: 'Berhasil menambahkan data field baru'})
    } catch (error) {
      return response.badRequest(error.messages)
    }
  }

  public async makeSchedule({request, response, params, auth}:HttpContextContract) {
    const data = await request.body()
    const {field_id, play_date_start, play_date_end} = data
    const venue_id = params.id

    // Check if venue is available
    const venue = await Venue
      .query()
      .where('id', venue_id)
    if (venue.length === 0) {
      return response.badRequest({message: "There's no such venue!"})
    }

    // Check if field is available in the venue
    const field = await Field
      .query()
      .preload('venue')
      .where('venue_id', venue_id)
      .where('id', field_id)
    if (field?.length === 0) {
      return response.badRequest({message: "There's no such field in this venue!"})
    }

    // Check if the schedule is available for a spacific time
    const schedule = await Booking
      .query()
      .preload('field')
      .where('field_id', field_id)
      .where('play_date_start', play_date_start)
      .where('play_date_end', play_date_end)
    if (schedule.length === 1) {
      return response.unprocessableEntity({message: 'Schedule has been taken!'})
    }

    const userId:any = auth.user?.id;

    const booking = new Booking()
    booking.play_date_start = play_date_start
    booking.play_date_end = play_date_end
    booking.fieldId = field_id
    booking.userId = parseInt(userId)

    const authUser = auth.user
    await authUser?.related('bookings').save(booking)
    return response.ok({message: 'Berhasil booking'})
  }
}
