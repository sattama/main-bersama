import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import  CreateFieldValidator  from "App/Validators/CreateFieldValidator";
import Field from 'App/Models/Field'

export default class FieldsController {
  public async index({response}:HttpContextContract) {
    const fields = await Field
      .query()
      .preload('venue', (query) => {
        query.select('name', 'address', 'phone')
      })
    return response.ok(fields)
  }

  public async show({params, response}:HttpContextContract) {
    let id = params.id
    const data = await Field
      .query()
      .preload('venue', (query) => {
        query.select('name','address','phone')
      })
      .preload('bookings', (query) => {
        query.select('id','play_date_start','play_date_end','user_id')
      })
      .where('id', id)
      .first()
    return response.ok(data);
  }

  public async update({params, response, request}:HttpContextContract) {
    let id = params.id
    try {
      const data = await request.validate(CreateFieldValidator)
      const [affectedRow] = await Field
        .query()
        .where('id', id)
        .update(data)
      return response.ok({message: "Data berhasil diupdate", data : affectedRow})
    } catch (error) {
      return response.badRequest(error.messages)
    }
  }

  public async destroy({params, response}:HttpContextContract) {
    let id = params.id
    const [affectedRow] = await Field.query().where('id', id).delete()
    if (affectedRow === 1) {
      return response.ok({message: "Field deleted!"})
    } else {
      return response.badRequest({message: "There's no such field in database!"})
    }
  }
}
