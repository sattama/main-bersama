import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UsersHasBookings extends BaseSchema {
  protected tableName = 'users_has_bookings'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('users_id').unsigned().references('users.id').onDelete('CASCADE')
      table.integer('bookings_id').unsigned().references('bookings.id').onDelete('CASCADE')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
