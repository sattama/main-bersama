import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AddUserIdToVenues extends BaseSchema {
  protected tableName = 'venues'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('users_id').unsigned().references('users.id').onDelete('CASCADE')
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn('user_id')
    })
  }
}
