Program ini adalah REST-API penyewaan tempat (venue) yang memiliki beberapa lapangan kepada berbagai user yang dapat melakukan join pada jadwal tertentu secara bersama-sama. User ini akan berbagi lapangan pada waktu yang telah mereka tentukan
Konsep dari program ini pada intinya adalah menyediakan aplikasi web untuk orang/sekelompok orang yang ingin berolahraga dengan menyewa suatu venue, tetapi masih membutuhkan orang lain untuk dapat menggunakannya secara bersama. Sehingga, dibuatlah program ini untuk menyelesaikan masalah para user yang ingin bermain bersama orang lain dengan menyewa vneue, tanpa harus khawatir kekurangan orang/pemain.
Adapun alur & fitur program sebagai berikut :
1. Registrasi
    User dapat melakukan registrasi dengan memasukkan nama, email, password, dan role (user atau owner)
2. Memasukkan kode OTP
    User harus memverifikasi OTP yang telah dikirimkan ke Email. Dalam implementasinya, email dikirimkan melalui mailtrap.
    Karena menggunakan swagger UI dan OTP dikirimkan hanya kepada akun mailtrap developer, maka OTP akan langsung dikirimkan saat itu juga ketika user berhasil melakukan registrasi.
    OTP akan dibutukan untuk mengakses berbagai endpoint pada REST API.
3. Login
    User yang sudah terverifikasi dapat melakukan login ke sistem
4. Membuat jadwal
    Untuk role user, dapat membuat jadwal pada waktu tertentu
5. Join/Unjoin jadwal
    User dapat melakukan join atau unjoin schedule dengan booking id tertentu
6. Melihat venue, field, jadwal
    User maupun owner dapat mendapatakan data venue, field, dan jadwal. Hanya untuk user tertentu yang login yang dapat melihat rincian jadwal yang diikuti
7. Memanipulasi venue, field, dan schedule
    Manipulasi untuk entitas tertentu hanya diberikan kepada role khusus yang dimiliki user.
    User dapat memanipulasi data jadwal. Owner dapat memanipulasi (create, update delete) data vield dan field

Nama : Satriya Adhitama
Instansi : Universitas Teknologi Yogyakarta
Tempat Tinggal : Klaten
Email : satadhitama@gmail.com
Kelas : Sanbercode Nodejs Back-end Batch 25
Trainer : Muhammad Abduh
Link Deploy : https://join-bareng.herokuapp.com/docs/index.html
